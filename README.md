### README

Este projeto contém uma implementação básica e funcional do Blockchain, além de um código para teste do Blockchain.

Existe também um documento, em PDF (Blockchain.pdf), que explica a razão de existir do Blockchain e sua estrutura. Na segunda parte deste documento se encontra o tutorial para implementação do Blockchain em javascript/Node, onde o resultado será igual ao deste projeto.