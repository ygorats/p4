const Blockchain = require('./blockchain')

const blockchain = new Blockchain()	// coloque a difficulty que desejar

blockchain.addBlock({ amount: 4 })	// criação de um bloco com dados fictícios
blockchain.addBlock({ amount: 50 })

console.log(blockchain)

console.log(blockchain.isValid())	// verificação do blockchain, retorna true porque seguiu o fluxo
blockchain.blocks[1].data.amount = 30000	// alteração dos dados de um bloco tornando-o inválido pois muda o hash gerado na verificação
console.log(blockchain.isValid())	// esta validação retorna false porque os dados de um bloco foram alterados